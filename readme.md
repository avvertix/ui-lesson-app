# UI-Lesson, example TODO desktop application

`//TODO` application built with [Electron](http://electron.atom.io)

The frontend is realized using [VueJS](http://vuejs.org/)

## Requirements

For development purposes the application requires [NodeJS](https://nodejs.org/en/) and NPM.

NPM is the package manager for the dependencies. It is installed by default by the NodeJS installer

To verify that all is configured you can execute on a Command Prompt (or shell), or on the Power shell

```
node --version
npm --version
```

## Building up the application

Before using the source code of the application as it is the dependencies must be pulled.

In this case the dependencies are

- Electron executable
- libraries, like VueJS

For this application the dependencies can be pulled using [NPM](https://docs.npmjs.com/cli/install) 
or [Yarn](https://yarnpkg.com/).

### Installing with NPM

To install the dependencies run

```
npm install
```

From the folder that contains this readme file. It will take some minutes, at the end you will have a
folder called `node_modules` with all the dependencies

### Installing with Yarn

Before using Yarn, you have to install it

```
npm install -g yarn
```

Then from the folder that contains this readme you can execute

```
yarn
```

At the end you will have the same `node_modules` with all the dependencies.


## Start

To start the application in development mode execute

```
npm start
```
