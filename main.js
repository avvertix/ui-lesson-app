'use strict';


const electron = require('electron');  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;
const app = electron.app;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the javascript object is GCed.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function () {

    if (process.platform != 'darwin') {
        app.quit();
    }

});

// This method will be called when Electron has done everything
// initialization and ready for creating browser windows.
app.on('ready', function () {
    
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1000,
        height: 800,
        minWidth: 420,
        minHeight: 400
        // icon: 'images/logo.png'
    });

    // and load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/index.html');
  
    // Open the devtools.
    mainWindow.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
});