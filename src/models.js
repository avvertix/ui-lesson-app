'use strict';

class Todo {
    
    /**
     * Create the TODO instance.
     * 
     * @param todo_params an object to initialize the todo, can have id, content, categories, done, dueDate
     * @returns Todo
     */
    constructor(todo_params){
        this.id = todo_params.id || new Date().getTime();
        this.content = todo_params.content || '';
        this.categories = [];
        this.done = todo_params.done || false;
        this.dueDate = todo_params.dueDate || null;
    }
    
    /**
     * Mark the todo as done
     */
    done() {
        this.done = true;
    }

    /**
     * Unmark the todo as done
     */
    undone() {
        this.done = false;
    }

    
}

module.exports.Todo = Todo;