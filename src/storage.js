
/**
 * Storage module
 * 
 * uses localStorage (not the best option if you have lots of data!!!)
 */
module.exports = (function () {

    var STORAGE_KEY = 'todos';


    return {

        fetch: function () {

            return JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');

        },

        save: function (todos) {

            localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));

        }

    };
})();