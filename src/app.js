(function (exports) {

	'use strict';

    /**
     * The handler that will store all our todos
     */
    const TodoStorage = require('./src/storage');

    const Todo = require('./src/models').Todo;

    /**
     * A very nice utility libraries for arrays, objects,...
     */
    const _ = require('lodash');
    // var shell = require('shell');

    const isTomorrow = require('date-fns/is_tomorrow');
    const isToday = require('date-fns/is_today');
	const dateFormat = require('date-fns/format');
	const startOfTomorrow = require('date-fns/start_of_tomorrow');



	var filters = {

		all: function (todos) {
			return todos;

		},

		active: function (todos) {

			return todos.filter(function (todo) {

				return !todo.done;

			});

		},

        today: function (todos) {

			return todos.filter(function (todo) {

				return !todo.done && isToday(todo.dueDate);

			});

		},

        tomorrow: function (todos) {

			return todos.filter(function (todo) {

				return !todo.done && isTomorrow(todo.dueDate);

			});

		},

		completed: function (todos) {

			return todos.filter(function (todo) {

				return todo.done;

			});

		}

	};



	exports.app = new Vue({



		// the root element that will be compiled

		el: '.js-todoapp',



		// app initial state

		data: {

			todos: TodoStorage.fetch(),

			newTodo: {content: '', dueDate: null},

			isCreateTodo: false,

			beforeEditCache: null,

			editedTodo: null,

			visibility: 'active',

			emptyBecauseCompletedAll: false

		},



		// watch todos change for localStorage persistence

		watch: {

			todos: {

				deep: true,

				handler: TodoStorage.save

			}

		},



		// computed properties

		// http://vuejs.org/guide/computed.html

		computed: {

			filteredTodos: function () {

				return filters[this.visibility](this.todos);

			},

			remaining: function () {

				return filters.active(this.todos).length;

			},

			allDone: {

				get: function () {

					return this.remaining === 0;

				},

				set: function (value) {

					this.todos.forEach(function (todo) {

						todo.done();

					});

				}

			}

		},



		// methods that implement data logic.

		// note there's no DOM manipulation here at all.

		methods: {

			createTodo: function(){

				this.newTodo.content = '';
				this.newTodo.dueDate = this.visibility === 'today' || this.visibility === 'tomorrow' ? dateFormat(this.visibility === 'tomorrow' ? startOfTomorrow() : new Date() , 'YYYY-MM-DD') : null ;
				this.isCreateTodo = true;

			},

			cancelCreateTodo: function(){

				this.isCreateTodo = false;

			},

			addTodo: function () {

				var value = this.newTodo.content && this.newTodo.content.trim();

				if (!value) {

					return;

				}

				this.todos.push(new Todo({ content: value, dueDate: this.newTodo.dueDate }));

				this.newTodo.content = '';
				this.newTodo.dueDate = null;

				this.isCreateTodo = false;

			},



			removeTodo: function (todo) {

                var index = this.todos.indexOf(todo);
                this.todos.splice(index, 1);

			},



			editTodo: function (todo) {

				this.beforeEditCache = {content: todo.content, dueDate: todo.dueDate};

				this.editedTodo = todo;

			},



			doneEdit: function (todo) {

				if (!this.editedTodo) {

					return;

				}
				
				todo.content = todo.content.trim();
				
				if (todo.content) {

					this.editedTodo = null;

				}

			},



			cancelEdit: function (todo) {

				this.editedTodo = null;

				todo.content = this.beforeEditCache.content;
				todo.dueDate = this.beforeEditCache.dueDate;

			},



			removeCompleted: function () {

				this.todos = filters.active(this.todos);

			}

		},


	});



})(window);